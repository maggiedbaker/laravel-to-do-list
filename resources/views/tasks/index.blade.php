@extends('layouts.app')

@section('content')

    <!-- Create Task Form... -->

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <h1>{{ Auth::user()->name }}'s To Do List</h1>
    </div>

     <!-- Current Tasks -->
    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    @if ($task->completed == true)
                                        <div><strike>{{ $task->name }} (Added {{ $task->getCreatedAt() }})</strike> Completed {{ $task->getUpdatedAt() }}</div>
                                        <!-- Undo Button -->
                                        <td>
                                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                {!! method_field('PUT') !!}

                                                <button type="submit" id="undo-task-completed{{ $task-> id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Undo
                                                </button>
                                            </form>
                                        </td>
                                    @else
                                        <div>{{ $task->name }} (Added {{ $task->getCreatedAt() }})</div>
                                        <!-- Completed Button -->
                                        <td>
                                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                                {!! csrf_field() !!}
                                                {!! method_field('PUT') !!}

                                                <button type="submit" id="mark-task-completed{{ $task-> id }}" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Mark as Completed
                                                </button>
                                            </form>
                                        </td>
                                    @endif
                                </td>

                                <!-- Delete Button -->
                                <td>
                                    <form action="{{ url('task/'.$task->id) }}" method="POST">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}

                                        <button type="submit" id="delete-task-{{ $task-> id }}" class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash"></i>Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif

        <!-- New Task Form -->
        <form action="{{ url('task') }}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}

            <!-- Task Name -->
            <div class="form-group">
                <label for="task-name" class="col-sm-3 control-label">Add a new item:</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="task-name" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="task-priority" class="col-sm-3 control-label">Priority:</label>

                <div class="col-sm-6">
                    <input type="radio" name="priority" value="low">Low<br>
                    <input type="radio" name="priority" value="medium">Medium<br>
                    <input type="radio" name="priority" value="high"> High
                </div>
            </div>

            <!-- Add Task Button -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i>Add to List
                    </button>
                </div>
            </div>
        </form>

<!--     <form class="form-horizontal" role="form" method="GET" action="{{ url('/logout') }}">
    {!! csrf_field() !!}
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-btn fa-sign-in"></i>Logout
        </button>
    </form> -->
    <a href="{{ URL::to('logout') }}">Logout</a>

@endsection