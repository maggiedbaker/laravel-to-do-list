<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['name', 'priority', 'completed?'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function getCreatedAt()
    {
      return $this->created_at->format('m-d-Y');
    }

    public function getUpdatedAt()
    {
      return $this->updated_at->format('m-d-Y');
    }

}
